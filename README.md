# Turkish Political Analysis - Public Opinion

This data set reveals the Turkish people's view towards political events. Machine-learning algorithms may be required to make forecasts based on the data. In conclusion, Random-Forest model can be seen how people's political thoughts have influenced the party they support.

***We have the following features in our dataset:***

*Cinsiyet:* Sex

*Yas:* Age

*Bolge:* Region in Turkey

*Egitim:* Education

*Soru1/Question1:* Do you think our Economic Status is good?

*Soru2/Question2:* Need Reform in Education?

*Soru3/Question3:* Resolve Privatization Are You?

*Soru4/Question4:* Should the state use a penalty like death penalty for certain crimes?

*Soru5/Question5:* Do you find our journalists neutral enough?

*Soru6/Question6:* From 22:00 hrs Then Are You Supporting the Prohibition to Buy Drinks?

*Soru7/Question7:* Do You Want to Live in a Secular State?

*Soru8/Question8:* Are you supporting the abortion ban?

*Soru9/Question9:* Do you think that the extraordinary state (Ohal) restricts Freedoms?

*Soru10/Question10:* Would you like a new part of the parliament to enter?

*Parti:* Political View

***Some Turkish to English conversions that might be useful:***

*Evet* = Yes

*Hayır* = No

*Erkek* = Male

*Kadın* = Female

*Yas* = Age

*Bolge* = City

*Egitim* = Education

*İlkokul* = Primary School

*OrtaOkul* = Middle School

*Lise* = High School

*Lisans* = License

*Ön Lisans* = Undergraduate (bachelor's)

*Lisans Üstü* = Graduate (master's)

**Summary of the Analysis:**

•	The dataset contains the information about the people with features as stated above. The original dataset contains categorical variables with no missing or null values.

•	The variable like ‘Timestamp’ is dropped because it serves no purpose in predicting the political party that a person supports.

•	Since the source of the data is Turkey, the column names are renamed to English language from a local language.

•	Most records support 4 major parties; 'IYI PARTI', 'CHP', 'DIĞER' and 'AKP'. So, we filter out the records corresponding to 'HDP' or 'MHP', because we don't have enough records to make any inference about them.

•	We convert the categorical (object) type columns to numerical (int) type.

•	From data visualization, we found out that there are more male respondents to this survey. The Age group ‘18-30’ is the most active age group across both genders. Within the Age group ‘18-30’, most males support the 'CHP' party, while female support the 'DIĞER' party.

•	Furthermore, among female, the Education level of 'License' is the most active age group on the survey. While among male, the Education level of 'High School' and 'License' are equally active in the survey. Within the Education level 'License', most males support the 'IYI PARTI' party, while female support the 'DIĞER' party.

•	For visualizing how different political views vary across supporters of different parties, I developed this cool interactive Tableau dashboard. Please visit, https://public.tableau.com/profile/ravinder3658#!/vizhome/Turkish_Political_Analysis/PoliticalViewsacrossPartysSupporters?publish=yes

•	The heatmaps are used to investigate the collinearity among the predictor variables, using the seaborn python library. There doesn't seems to be a problem of strong collinearity among the predictor variables.

•	In the building models process, the data is divided into train and test splits with 95-5% proportions.

•	8 different machine learning regression models like SGDClassifier, RandomForestClassifier, LogisticsRegression, KNeighborsClassifier, Gaussian Naive Bayes, Perceptron, LinearSVC, DecisionTreeClassifier are fitted to the training data. Different models are compared based on the 3 different accuracy metrics (on training set - 95%, on test set - 5%, and mean20CV - on full data).

•	It was found out that the random forest classifier achieved the highest accuracy value across all 3 metrics, with decision tree classifier achieving the second best position.

•	The feature importance’s of the predictor variables revealed that the variables 'Journalists_Neutral_Enough?' and 'Need_Education_Reform?' has very low feature importance. So, we decided to drop them, as they do not help much in decreasing the impurity of the classes. Then, RandomForestClassifier is fit on the new set of predictor variables. There was a slight increase in the mean20CV accuracy for the new model.

•	The hyperparameters like ‘criterion’, ‘min_samples_leaf’, ‘min_samples_split’ and ‘n_estimators’ are tuned using the GridSearchCV. The tuned model achieved a higher oob_score.

•	The final model’s performance achieved 89% on train and 75% accuracy on test data.

•	Finally visualized individual decision trees from our final random forest.

***Techniques Used:*** SGDClassifier, RandomForestClassifier, LogisticsRegression, KNeighborsClassifier, Gaussian Naive Bayes, Perceptron, LinearSVC, DecisionTreeClassifier, GridSearchCV, seaborn, sklearn. (**Python**, **Tableau**)


